import Home from './views/Home.vue'
import Login from './views/Login.vue'
// import Header from './components/HeaderSDMS.vue'
import { createRouter, createWebHistory } from 'vue-router'

const routes = [
    {
        name: 'Login',
        component: Login,
        path: '/'

    },
    {
        name: 'Home',
        component: Home,
        path: '/Home'

    }
    
];
const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;

