import { createApp } from 'vue'
import { createStore } from 'vuex'
import App from './App.vue'
import router from './routers'

import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap/dist/js/bootstrap.js"


// Create a new store instance.
const store = createStore({
    state() {
        return {
            urlH1FE: "https://sdmsuat.mpm-motor.com",
            urlH23FE: "https://sdmsuat-h23.mpm-motor.com",
            urlH1API: "https://sdmsuat.mpm-motor.com:8084",
            urlH23API: "https://sdmsuat-h23.mpm-motor.com:8323",
        }
    },
    // mutations: {
    //   increment (state) {
    //     state.count++,
    //     state.url = "ANC"
    //   }
    // },
    getters: {
        urlH1FE(state) {
            return state.urlH1FE
        },
        urlH1API(state) {
            return state.urlH1API
        },
        urlH23API(state) {
            return state.urlH23API
        },
        urlH23FE(state) {
            return state.urlH23FE
        },
    }
})

const app = createApp(App)

app.use(store).use(router).mount('#app');

// createApp(App)
//     .use(router)
//     .use(store)
//     .mount('#app'); 
